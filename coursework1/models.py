from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError, FieldError


CATEGORY_CHOICES = (
    ('pol', 'Politics'),
    ('art', 'Art'),
    ('tech', 'Technology'),
    ('trivia', 'Trivial News')
)
REGION_CHOICES = (
    ('uk', 'United Kingdom'),
    ('eu', 'Europe'),
    ('w', 'Worldwide')
)


class Author(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    name = models.CharField(max_length=30)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Author.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.author.save()


class Story(models.Model):
    headline = models.CharField(max_length = 64)
    category = models.CharField(max_length = 6, choices = CATEGORY_CHOICES)
    region = models.CharField(max_length = 2, choices = REGION_CHOICES)
    author = models.ForeignKey(Author, on_delete = models.CASCADE)
    date = models.DateField(auto_now = True)
    details = models.CharField(max_length = 512)

    def clean(self):
        if len(self.headline) > 64:
            raise ValidationError('Headline is too long')
        print(self.category)
        if self.category not in dict(CATEGORY_CHOICES):
            raise ValidationError("Invalid Category Choice")
        if str(self.region) not in dict(REGION_CHOICES):
            raise ValidationError("Invalid Region Choice")
        if len(self.details) > 512:
            raise ValidationError("Details too long")
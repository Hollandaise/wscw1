from django.contrib import admin
from .models import Author, Story

class AuthorAdmin(admin.ModelAdmin):
    list_display = ('user', 'name')

class StoryAdmin(admin.ModelAdmin):
    list_display = ('category', 'region', 'author','date')

admin.site.register(Author,AuthorAdmin)
admin.site.register(Story,StoryAdmin)
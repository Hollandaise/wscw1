from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError, FieldError
#Addition library - pip install python-dateutil
from dateutil import parser
import datetime
import json
from .models import Story, CATEGORY_CHOICES, REGION_CHOICES
from django.core.serializers import serialize, deserialize


def index(request):
    return HttpResponse("Welcome to this test site implementation. Feel free to send REST requests!")

@csrf_exempt
def login_view(request):
    if request.method == 'POST':
        # grab form data
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)  # Create new session
            responseStr = "Welcome: " + user.author.name
            return HttpResponse(responseStr, status=200)
        else:
            return HttpResponse("There was a problem with logging in.", status=400, content_type='text/plain')
    else:
        return HttpResponse("Only Post Requests Are Accepted", status=400, content_type='text/plain')


@csrf_exempt
def logout_view(request):
    if request.method == 'POST':
        #Check the user is logged in first
        user = request.user
        if user.is_authenticated:
            logout(request)
            return HttpResponse("You have been logged out." , status=200, content_type='text/plain')
        else:
            return HttpResponse("You are not logged in. You must be logged in to log out." , status=503, content_type='text/plain')
    else:
        return HttpResponse("Only Post Requests Are Accepted", status=400, content_type='text/plain')


@csrf_exempt
def postStory(request):
    if request.method == 'POST':
        user = request.user
        if user.is_authenticated:
            # Avoid parsing anything other than JSON data
            if request.content_type == 'application/json':
                recievedJson = json.loads(request.body.decode('utf-8'))
                result, errReason = addStoryToDB(recievedJson, user)
                if not result:
                    return HttpResponse(errReason, status=503)
                return HttpResponse("Story Submitted!", status=201, content_type='text/plain')
            return HttpResponse("Invalid Content Format.", status=415, content_type='text/plain')
        else:
            return HttpResponse("You are not logged in. You must be logged in to post stories", status=503, content_type='text/plain')
    else:
        return HttpResponse("Only Post Requests Are Accepted", status=503, content_type='text/plain')


@csrf_exempt
def getStories(request):
    if request.method != "GET":
        return HttpResponse("Only Get Requests Are Accepted", status=503, content_type='text/plain')
    # if request.content_type != 'application/json':
    #     return HttpResponse("Invalid Content Format",status=415, content_type='text/plain')

    jsonData = json.loads(request.body.decode('utf-8'))
    results = None
    error = "No Results Found"
    #Prevent JSON exceptions
    if 'story_date' in jsonData and 'story_cat' in jsonData and 'story_region' in jsonData:
        #First assume no custom filters
        results = Story.objects.all()

        date = jsonData['story_date']
        category = jsonData['story_cat']
        region = jsonData['story_region']

        #Filter down already retrieved data if needed
        if date != "*":
            try:
                parsedDate = parser.parse(jsonData['story_date'])
                dateStr = parsedDate.strftime("%Y-%m-%d")
                results = results.filter(date__gte=dateStr)
            except ValueError:
                error = "Invalid Date String"

        if category != "*":
            results = results.filter(category=category)

        if region != "*":
            results = results.filter(region=region)

        if list(results):
            parsedResults = dumpResults(list(results))
            # jsonRes = json.dumps(parsedResults)
            return JsonResponse(parsedResults,safe=False, status=200)
    # Blanket 404 response
    return HttpResponse(f'{error}', status=404, content_type='text/plain')

@csrf_exempt
def deleteStory(request):
    err = None
    if request.method == 'POST':
        user = request.user
        if user.is_authenticated:
            jsonData = json.loads(request.body.decode('utf-8'))
            if "story_key" in jsonData:
                idToDelete = jsonData["story_key"]
                results = Story.objects.filter(id=idToDelete)
                if results:
                    results.delete()
                    return HttpResponse("Entry Deleted", status=201, content_type='text/plain')
                else:
                    err = "Entry with corresponding ID not found."
            else: err = "ID Missing from JSON Request."
        else:
            err = "You are not logged in."
    else:
        err = "This service will only service POST Requests"
    return HttpResponse(f'Error! Reason: {err} ', status=503, content_type='text/plain')



#Validate the DB entry before adding - allows us to customise the error response based on exception
def addStoryToDB(jsonData, user):
    try:
        newEntry = Story(headline=jsonData['headline'], category=jsonData['category'], region=jsonData['region'],
                         details=jsonData['details'], author=user.author)
        newEntry.clean()
        newEntry.save()
        return True, None
    except ValidationError as e:
        return False, e
    except FieldError:
        return False, "Error Adding to DB"
    except KeyError as e:
        return False, f'Missing Attribute {e}'


def dumpResults(resultList):
    jsonToReturn = {}
    storyArray = []
    #Convert local form of json into required response
    for result in resultList:
        newStory = {}
        newStory["author"] = result.author.name
        newStory["headline"] = result.headline
        newStory["key"] = result.id
        newStory["story_cat"] = dict(CATEGORY_CHOICES).get(result.category)
        newStory["story_date"] = result.date.strftime("%d/%m/%Y")
        newStory["story_region"] = dict(REGION_CHOICES).get(result.region)
        newStory["story_details"] = result.details
        storyArray.append(newStory)
    jsonToReturn["stories"] = storyArray
    # print(jsonToReturn)
    return jsonToReturn

import requests
import json
from urllib.parse import urlparse
import re
import getpass
from pandas.io.json import json_normalize
import textwrap
main_session = requests.session()
current_login = None

def main():
    print ("Welcome to the JAP news agency!")
    while True:
        try:
            cmd = input("Input your command:\n")
            cmd = cmd.split()
            main_cmd = cmd[0].upper()
    
            if main_cmd == "LOGIN":
                login(cmd[1:])
                
            elif main_cmd == "POST":
                post_story()
                
            elif main_cmd == "NEWS":
                get_news(cmd[1:])
                
            elif main_cmd == "LOGOUT":
                logout()
        
            elif main_cmd == "LIST":
                list_stories()
                
            elif main_cmd == "DELETE":
                delete_story(cmd[1:])
                
            elif main_cmd == "HELP":
                print_help()
                
            elif main_cmd == "EXIT":
                print("Goodbye!")
                exit()
            else:
                print("Command not recognised")
                print_help()
        except requests.exceptions.ConnectionError:
            print("We could not connect to the client!")
        except IndexError:
            print("Please Provide an Argument")

def login(params):
    global current_login
    if current_login:
        print ("You are already logged in")
        return False
    if not params:
        print ("No URL Provided!")
        return False
    url = params[0]
    if '//' not in url:
        url = "http://" + url
    login_url = url + '/api/login/'
    un = input("Username: ")
    pw = getpass.getpass()
    login_form = {'username':un, 'password':pw}
    login_request = main_session.post(login_url, data = login_form)
    print (login_request.text)
    if login_request.status_code == 200:
        current_login = url
        return True
    return False

def logout():
    global current_login
    if current_login:
        url = current_login + "/api/logout/"
        response = main_session.post(url)
        print(response.text)
        if response.status_code == 200:
            current_login = None

def delete_story(args):
    global current_login
    if current_login:
        key = args[0]
        url = current_login + "/api/deletestory/"
        params = dict(
            story_key=key
        )
        resp = main_session.post(url=url, json=params)
        print(resp.text)
        return True
    print("You need to be logged in first!")
    return False


def list_stories():
    agencyResp = main_session.get("http://directory.pythonanywhere.com/api/list/")
    # # parsed_json = agencyResp.json()
    # parsed = json.loads(agencyResp.text)
    # print(json.dumps(parsed, indent=4, sort_keys=True))
    print(json_normalize(agencyResp.json(),'agency_list'))
    return True
    
def print_help():
    print("HELP")

def post_story():
    global current_login
    if current_login:
        url = current_login + "/api/poststory/"
        headline = input("Enter Headline: ")
        cat = None
        while not cat:
            val_category = input("Select Category:\n1)Politics\n2)Art\n3)Technology\n4)Trivia\nSelect Option: ")
            if val_category == '1':
                cat = 'pol'
            elif val_category == '2':
                cat = 'art'
            elif val_category == '3':
                cat = 'tech'
            elif val_category == '4':
                cat = 'trivia'
            else:
                print("Your selection was invalid? Try again")
                #Might change this into an input so we can change our minds later
        region = None
        while not region:
            val_category = input("Select Region:\n1)United Kingdom\n2)Europe\n3)World\nSelect Option:")
            if val_category == '1':
                region = 'uk'
            elif val_category == '2':
                region = 'eu'
            elif val_category == '3':
                region = 'w'
            else:
                print("Your selection was invalid? Try again")
                #Might change this into an input so we can change our minds later
        details = input("Write Details: ")
        params = dict(
            headline=headline,
            category=cat,
            region=region,
            details=details
        )
        resp = main_session.post(url=url, json=params)
        print(resp.text)# Check the JSON Response Content documentation below
        return True
    print("You need to be logged in first!")
    return False
        
            

def get_news(args):
    try:
        check_id = search_news_args(args, "-id=")
        print(check_id)
        check_cat = search_news_args(args, "-cat=")
        print(check_cat)
        check_reg = search_news_args(args, "-reg=")
        print(check_reg)
        check_date = search_news_args(args,"-date=")
        print(check_date)
        agencyResp = main_session.get("http://directory.pythonanywhere.com/api/list/")
        parsed_json = agencyResp.json()
        for agency in parsed_json["agency_list"]:
            ag_name = agency["agency_name"]
            ag_url = agency["url"].rstrip('/') + "/api/getstories/"
            # print(ag_url)
            ag_code = agency["agency_code"]
            if check_id is not '*' and ag_code == check_id:
                print("Searching " + ag_name)
                params = dict(
                    story_region=check_reg,
                    story_cat=check_cat,
                    story_date=check_date,
                )
                resp = main_session.get(url = ag_url, json = params)
                if resp.status_code == 200:
                    try:
                        printStories(resp.json())
                    except TypeError:
                        continue
                return True
            if check_id is '*':
                params = dict(
                    story_region=check_reg,
                    story_cat=check_cat,
                    story_date=check_date,
                )
                resp = main_session.get(url = ag_url, json = params)
                if resp.status_code == 200:
                    try:
                        printStories(resp.json())
                    except TypeError:
                        continue


            else:
                continue
        return True
    except ValueError as e:
        print(e)
        return False
    


def search_news_args(args, searchStr):
    if len(args) > 4:
        raise ValueError("Too many arguments supplied")
    for arg in args:
        if arg.startswith(searchStr):
            returnStr = arg.split('=')[1]
            return returnStr.strip('\"')
    return "*"

def printStories(j):
    bold = '\033[1m'
    end = '\033[0m'
    for i in j["stories"]:
            try:
                keyStr = (bold + " Story Key: " + end + str(i["key"]) + " ")
                headStr = bold + "Headline: " + end + i["headline"]
                catStr = bold + "Story Category: " + end + i["story_cat"]
                dateStr = bold +"Story Date: " + end + i["story_date"]
                regStr = bold + "Story Region: " +end + i["story_region"]
                detStr = textwrap.fill(bold + "Story Details: " + end + i["story_details"], 80)
                authStr = bold + "Author: " +end + i["author"]
            except (TypeError,KeyError):
                continue
            print(keyStr.center(80, '*'))
            print(headStr)
            print(authStr)
            print(dateStr)
            print(catStr)
            print(regStr)
            print(detStr)
            print(''.center(80, '*'))
            print()
            
        

if __name__ == "__main__":
    main()
# payload = {'username': 'sc15j3h', 'password':'alpine123'}
# login_request = sess.post("http://127.0.0.1:8000/api/login/", data = payload)
# print(login_request.text)

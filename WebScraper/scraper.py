import requests
from bs4 import BeautifulSoup
import json
import time
import re
import urllib.robotparser

INVERTED_DB = None
ROOT_URL = "http://example.webscraping.com"
# TODO: Start Search At Root of site
# Session object just in case we want to have a persis
s = requests.Session()


class InvertedIndex:
    def __init__(self):
        self.db = {}

    def add_to_index(self, word, url):
        # Strip all characters in a word that are not alpha-numeric (hyphenated words are legal)
        sanitised_word = re.sub('[^A-Za-z0-9\-]+', '', word)

        if sanitised_word:
            print(sanitised_word)
            if sanitised_word in self.db:
                current_word = self.db[sanitised_word]
                if url in current_word:
                    current_word[url] += 1
                else:
                    current_word[url] = 1
            else:
                self.db[sanitised_word] = {url: 1}

    def return_inv_index(self, key_word):
        key_entry = self.db[key_word]
        try:
            for key, value in key_entry.items():
                print("URL: " + key)
                print("Occurrences: " + str(value) + "\n")
        except KeyError:
            print("No Entries Found.")

    def get_link_list(self, word):
        try:
            # Dictionaries have unique key instances anyway, but a set needs to be returned for intersection
            return set(self.db[word].keys())
        except KeyError:
            return set([])

    def find_entries(self, user_args):
        set_list = []
        for arg in user_args:
            set_list.append(self.get_link_list(arg))
        # Set intersection will return links that exists in each word's instance.
        results_list = set.intersection(*set_list)
        if results_list:
            print("Results:")
            for result in results_list:
                print(result)
        else:
            print("No results found")


class Crawler:
    def __init__(self):
        self.inv_index = InvertedIndex()
        self.rp = urllib.robotparser.RobotFileParser()
        # Read the robots file and extract the courtesy window
        self.rp.set_url(ROOT_URL + '/robots.txt')
        self.rp.read()
        self.timeout = self.rp.crawl_delay("*")
        # URLs we want to start the crawl from (/ = /places/default/index in our example)
        self.crawl_list = ['/places/default/index']

    def crawl_site(self):
        for page in self.crawl_list:
            current_page = ROOT_URL + page
            print("Searching Page: " + current_page)
            r = s.get(current_page)
            soup = BeautifulSoup(r.text, "html.parser")
            header = soup.find("div", {"class": "page-header"})
            title = header.find('h1')
            if title:
                if title.text:
                    for word in title.text.strip().split(' '):
                        self.inv_index.add_to_index(word, current_page)
            subtitle = header.find('h2')

            if subtitle:
                if subtitle.text:
                    for word in subtitle.text.strip().split(' '):
                        self.inv_index.add_to_index(word, current_page)
                        # print(word)

            for link in soup.find_all('a'):
                relTag = link.get('rel')
                if relTag is None or 'nofollow' not in relTag:
                    link_to_add = link.get('href')
                    link_str = ROOT_URL + link_to_add
                    # /edit/ links should be nofollow via intrinsic association of /login/
                    if self.rp.can_fetch("*", link_str) \
                            and link_to_add not in self.crawl_list \
                            and '/edit/' not in link_to_add:
                        self.crawl_list.append(link_to_add)

            for table_element in soup.findAll('td'):
                if table_element.parent.get('id') != 'places_postal_code_regex__row':
                    if table_element.text:
                        for word in table_element.text.strip().split(' '):
                            if word.replace(',', '').isdigit() or len(word.split(',')) == 1:
                                self.inv_index.add_to_index(word, current_page)
                            else:
                                for sub_word in word.split(','):
                                    self.inv_index.add_to_index(sub_word, current_page)

            time.sleep(self.timeout)

    def export_index(self):
        with open('invIndex.json', 'w') as fp:
            json.dump(self.inv_index.db, fp)
            fp.close()


def load_entries():
    global INVERTED_DB
    indexToLoad = InvertedIndex()
    with open('invIndex.json', 'r') as fp:
        try:
            # json library can parse file straight to a dictionary.
            indexToLoad.db = json.load(fp)
            INVERTED_DB = indexToLoad
        except json.JSONDecodeError:
            print("Could not load the inverted index")
        finally:
            fp.close()


def main():
    print("Welcome!")
    is_running = True
    while is_running:
        try:
            user_in = input("Please Input a Command: ")
            if user_in:
                user_args = user_in.split(' ')
                cmd = user_args[0].upper()

                if cmd == "BUILD":
                    print("Building Inverted Index. Please Wait...")
                    crawler = Crawler()
                    crawler.crawl_site()
                    crawler.export_index()
                    load_entries()
                    print("Done!")

                elif cmd == "LOAD":
                    print("Loading Inverted Index...")
                    load_entries()
                    print("Done")

                elif cmd == "PRINT":
                    if INVERTED_DB is not None:
                        if user_args[1]:
                            INVERTED_DB.return_inv_index(user_args[1])
                        else:
                            print("Search term is empty!")
                    else:
                        print("Inverted Index has not been loaded, please execute 'load' or 'build' first")

                elif cmd == "FIND":
                    if INVERTED_DB is not None:
                        if len(user_args) > 1:
                            INVERTED_DB.find_entries(user_args[1:])
                        else:
                            print("Search term is empty")
                    else:
                        print("Inverted Index has not been loaded, please execute 'load' or 'build' first")

                elif cmd == "EXIT":
                    is_running = False
        except KeyboardInterrupt:
            is_running = False
    print("\nGoodbye!\n")


if __name__ == '__main__':
    main()
